import numpy as np
import matplotlib as mpl
mpl.use('TkAgg')
import matplotlib.pyplot as plt
plt.ion()
import sys
import pandas

mpl.rcParams.update({'figure.figsize': (15,9),
                     'text.usetex': True,
                     'font.family': 'serif',
                     # 'font.serif': 'Georgia',
                     # 'mathtext.fontset': 'cm',
                     'lines.linewidth': 2.5,
                     'font.size': 14,
                     'xtick.labelsize': 'large',
                     'ytick.labelsize': 'large',
                     'legend.fancybox': True,
                     'legend.fontsize': 18,
                     'legend.framealpha': 0.7,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'savefig.dpi': 80,
                     'pdf.compression': 9})

headers = ['time','voltage']
#
# pzt_strain_1 = pandas.read_csv('back_mirror_normal_full_pzt_strain.csv',names=headers,usecols=[3,4])
# michelson_fringe_1 = pandas.read_csv('back_mirror_normal_full_fringe.csv',names=headers,usecols=[3,4])
#
# pzt_strain_2 = pandas.read_csv('back_mirror_extra_torque_full_pzt_strain.csv',names=headers,usecols=[3,4])
# michelson_fringe_2 = pandas.read_csv('back_mirror_extra_torque_full_fringe.csv',names=headers,usecols=[3,4])
#
# front_pzt_strain_1 = pandas.read_csv('front_mirror_normal_full_pzt_strain.csv',names=headers,usecols=[3,4])
# front_michelson_fringe_1 = pandas.read_csv('front_mirror_normal_full_fringe.csv',names=headers,usecols=[3,4])
#
fringe1 = pandas.read_csv('data40inlbsPhotodiodeVoltage.csv',names=headers,usecols=[3,4])
pzt_v1 = pandas.read_csv('data40inlbsPZTVoltage.csv',names=headers,usecols=[3,4])

fringe2 = pandas.read_csv('data50inlbsPhotodiodeVoltage.csv',names=headers,usecols=[3,4])
pzt_v2 = pandas.read_csv('data50inlbsPZTVoltage.csv',names=headers,usecols=[3,4])

plt.subplot(211)
plt.plot(fringe1['time'], fringe1['voltage'], 'C1', label='PD, 40 in lbs')
plt.plot(fringe2['time'], fringe2['voltage'], 'C2', label='PD, 50 in lbs')
plt.xlabel('Time [s]')
plt.ylabel('Michelson fringes, antisymmetric port [V]')
plt.title('0-100V SAMS PZT scan')
#plt.xlim(-0.3, 0.3)
plt.legend()
plt.grid()
plt.tight_layout()

plt.subplot(212)
plt.plot(pzt_v1['time'], pzt_v1['voltage'], 'C1', label='PZT, 40 in lbs')
plt.plot(pzt_v2['time'], pzt_v2['voltage'], 'C2', label='PZT, 50 in lbs')
plt.xlabel('Time [s]')
plt.ylabel('PZT voltage [V]')
#plt.title('0-100V SAMS PZT scan, 40 in lbs base torque')
#plt.xlim(-0.3, 0.3)
plt.legend()
plt.grid()
plt.tight_layout()

plt.savefig('newData.pdf')
