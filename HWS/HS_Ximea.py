from numpy import *
import cv2
from HS_Image import *
from HSM_Utils import save_image_frame, read_image_frame

class HS_Ximea:

    def __init__(self, device_no=0, to_connect=True, master_folder=None):
        self.vc = None
        self.device_sn = None
        self.camera_folder = None
        self.default_exposure_time = 1000
        self.exposure_time = None
        self.sensor_temperature = None
        self.gain = None
        self.master_folder = "D:/Cabinet/Images/Ximea/"
        self.frame_folder = None

        if to_connect is True:
            self.connect(device_no)
            # self.set_camera_folder()

    def set_camera_folder(self):
        """Set up the default camera folder in which to store data.

        :Updates: ``camera_folder``

        """
        csn = self.device_sn
        cf = HS_Ximea.master_folder + csn

        if os.path.exists(HS_Ximea.master_folder):
            if not os.path.exists(cf):
                print('The camera folder ' + csn + ' does not exist.' + \
                      'Creating...')
                os.makedirs(cf)
                print('Created the camera folder: ' + cf + '/')
        else:
            print('The master folder ' + HS_Ximea.master_folder + \
                  ' does not exist. ' + 'Creating...')
            os.makedirs(HS_Ximea.master_folder)
            print('Ceated the master folder: ' + HS_Ximea.master_folder)
            os.makedirs(cf)
            print('Created the camera folder: ' + cf)

        self.camera_folder = cf + '/'

    def connect(self, device_no=0, to_reset=True):
        if self.exposure_time is None:
            prv_et = self.default_exposure_time
        else:
            prv_et = self.exposure_time

        self.vc = cv2.VideoCapture(device_no)
        self.device_sn = int(self.vc.get(cv2.CAP_PROP_XI_DEVICE_SN))
        self.reset_settings()

        # In future, there may be more settings than just exposure time to
        # keep from resetting. If so, the code block below should be
        # expanded.
        if to_reset is False:            
            self.set_exposure_time(prv_et)
        else:
            self.set_exposure_time(self.default_exposure_time)

    def reset_settings(self):
        # This method resets some settings to what we want, not the
        # manufacturer default settings.

        # IMAGE_DATA_FORMAT must be set before setting BIT_DEPTH
        # parameters. Otherwise changing bit depths to 10 may not work and
        # revert back to factory default 8 bit.
        self.vc.set(cv2.CAP_PROP_XI_IMAGE_DATA_FORMAT,6)
        self.vc.set(cv2.CAP_PROP_XI_SENSOR_DATA_BIT_DEPTH,10)
        self.vc.set(cv2.CAP_PROP_XI_IMAGE_DATA_BIT_DEPTH,10)
        # turn off automatic exposure/gain control
        self.vc.set(cv2.CAP_PROP_XI_AEAG,0)
        # turn off bad pixel correction
        self.vc.set(cv2.CAP_PROP_XI_BPC,0)
        # turn off column fpn correction
        self.vc.set(cv2.CAP_PROP_XI_COLUMN_FPN_CORRECTION,0)
        # turn off row fpn correction
        self.vc.set(cv2.CAP_PROP_XI_ROW_FPN_CORRECTION,0)
        self.set_gain(0)
        self.get_sensor_temperature()


    def take_frame(self):
        flag, im = self.vc.read()
        return cv2.flip(im, flipCode=-1)

    def take_and_save_frame(self, name):
        im = self.take_frame()
        save_image_frame(name, im)
        # since take_frame already flips the acquired image, should not flip again here.        
        # return cv2.flip(im)
        return im

    def take_and_average_frames(self,
                                save_frames=False,
                                nof=1,
                                start_no=1,
                                folder=None,
                                fprefix='image'):
        im = zeros(shape(self.take_frame()))

        if save_frames is True:
            fp_list = self.generate_file_names(nof, start_no, fprefix)

            if folder is None:
                folder = self.frame_folder

            for nn in xrange(nof):
                fn = folder + fp_list[nn]
                im = im + self.take_and_save_frame(fn)
        else:
            im = im + self.take_frame()

        return im/float(nof)

    def take_and_locate_bad_pixels(self,
                                   pv_threshold=150,
                                   no_of_frames=100):
        """Take the image frames and locate pixels whose values are higher than
        the specified threshold value.

        The method updates the instance variable ``bad_pixels_data`` which is a
        dict of several fields; for details see the description of the variable.

        The method also saves ``bad_pixel_data`` as a file.

        :param pv_threshold: the threshold pixel value to determine bad pixels
        :type pv_threshold: *float*
        :param no_of_frames: the number of image frames to take
        :type no_of_frames: positive *integer*
        :param nbuf: the number of ring buffers to use when taking the frames
        :Requires: ``camera_parameters.camera_serial_number``, ``camera_folder``,
                   :class:`HS_Image <HWS.HS_Image.HS_Image>`
        :Uses: :func:`~HS_Ximea.take_and_average_frames`,
               :func:`HS_Image.locate_bad_pixels() <HWS.HS_Image.HS_Image.locate_bad_pixels>`
        :Updates: ``bad_pixels_data``

        """
        csn = self.device_sn
        folder = self.camera_folder + 'dark_images/'
        nof = no_of_frames
        nbf = nbuf
        hsi = self.take_and_average_frames(folder,
                                           fprefix='dark',
                                           no_of_frames=nof)
        hsi.locate_bad_pixels(pv_threshold)

        bad_pixels_data = {}
        bad_pixels_data['csn'] = csn
        bad_pixels_data['pv_threshold'] = pv_threshold
        bad_pixels_data['coords'] = hsi.bad_pixels
        bad_pixels_data['gpstime'] = self.gpstime
        bad_pixels_data['exposure_time'] = self.get_exposure_time()

        bp_fileloc = self.camera_folder + 'bad_pixels_data_' + csn
        save(bp_fileloc,bad_pixels_data)

        self.bad_pixels_data = bad_pixels_data

        print('Saved bad pixels data as a file ' + \
              'bad_pixels_data_' + csn + '.npy ' + \
              'in the folder ' + self.camera_folder)
        return hsi

    def generate_file_names(self, nof=1, start_no=1, prefix='image', fext='tiff'):
        fp_list = []
        # maximun zero padding length: default is 4 and can be larger if necessary
        zp = max(len(str(nof + start_no -1)), 4)

        for nn in xrange(nof):
            fidx = nn + start_no
            fp_list.append(prefix + str(fidx).rjust(zp, '0') + '.' + fext)

        return fp_list

    def get_sensor_temperature(self, to_output=False):
        self.sensor_temperature = \
          self.vc.get(cv2.CAP_PROP_XI_SENSOR_BOARD_TEMP)

        if to_output is True:
            return self.sensor_temperature

    def get_exposure_time(self, to_output=False):
        self.exposure_time = self.vc.get(cv2.CAP_PROP_XI_EXPOSURE)

        if to_output is True:
            return self.exposure_time

    def set_exposure_time(self, et):
        self.vc.set(cv2.CAP_PROP_XI_EXPOSURE,et)
        self.get_exposure_time()

    def get_gain(self, to_output=False):
        self.gain = self.vc.get(cv2.CAP_PROP_XI_GAIN)

        if to_output is True:
            return self.gain

    def set_gain(self, gain):
        self.vc.set(cv2.CAP_PROP_XI_GAIN,gain)
        self.get_gain()

    def release(self):
        self.vc.release()


if __name__ == '__main__':

    try: 
        print 'Testing...'
        cam = HS_Ximea()
        print cam.device_sn
        print cam.vc.get(cv2.CAP_PROP_XI_SENSOR_BOARD_TEMP)
        # verify directly acquired frame and the frame saved then read from
        # file have the same pixel values.
        fn = 'D:/Cabinet/Images/Ximeatest_pv.tiff'
    
        for nn in arange(10):
            oim = cam.take_and_save_frame(fn)
            sim = read_image_frame(fn)
            dim = oim - sim
            print oim.max(), oim.min(), sim.max(), sim.min(), dim.max(), dim.min()
    except:
        cam.release()
        raise

    cam.release()

# Won Kim 9/08/2018 3:44:48 PM

