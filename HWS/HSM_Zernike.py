import sys
import os

hspath = os.path.abspath('')
if hspath not in sys.path:
    sys.path.append(hspath)
    
from HS_Centroids import *
from HS_Gradients import *
import numpy as np
import matplotlib.pyplot as plt
from scipy import special as sc
from scipy import interpolate
import matplotlib


def cart2pol(x, y):
    rho = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    return(rho, phi)
def Zernike_nm_indices(i):
    iarr = np.arange(1,i+1,1)
    nroot= np.roots([1,1,-2*(i+1)])
    nroot = nroot[nroot>0]
    nmax = int(np.floor(nroot))
    # order of mode in max n:
    nmodem = int(np.floor((nroot-nmax)*(nmax+2)))
    # Generate n coeff array up to nmax:
    n = np.zeros([0,1])
    for iN in range(1,nmax+1):
        n = np.vstack([n,iN*np.ones([iN+1,1])])
    if nmodem != 0:
        n = np.vstack([n,(nmax+1)*np.ones([nmodem,1])])
    # Generate corresponding m indices vector:
    m = np.zeros(np.shape(iarr))
    for iI in iarr:
        m[iI-1] =  (iI*2) -n[iI-1]*(n[iI-1]+2)
    return n,m


def Nnm(n,m):
    # Return normalization coefficient a given Znm:
    Nnm = np.sqrt((2-np.equal(0,m).astype(int8))*(n+1))
    return Nnm

def Rnm_coeff(n,m):
    # Find coeeficient to evvaluate sum of radial term in zernike m,n order
    m = np.abs(m)
    smax = int((n-m)/2 )
    Rc = np.zeros([smax+1,1]) 
    for iS in range(smax+1):
        Rc[iS] = ((-1)**iS * sc.factorial(n-iS))/(sc.factorial(iS)*sc.factorial((n+m)/2-iS)*sc.factorial((n-m)/2-iS) )
    return Rc
def Rnm(rho,n,m):
    # Compute radial term of Zenike order m,n
    Rc = Rnm_coeff(n,m)
    Rmn = np.zeros(np.shape(rho))
    for ic,iS in enumerate(Rc):
        Rmn += iS*rho**(n-2*ic)
    return Rmn


def Theta_nm(theta,m):
    # Compute azimuthal part of Zernike:
    if m >= 0:
        Theta_mn = np.cos(m*theta)
    else:
        Theta_mn = -np.sin(m*theta)
    return Theta_mn



def Znm(rho,theta,n,m):
    # Compute Zernike polynmial of order n,m 
    Znm = Nnm(n,m)*Rnm(rho,n,m)*Theta_nm(theta,m)
    return Znm

def Zderivs_Znm(rho,theta,n,m):
    # Return first terms in recursive relationship
    a = np.sign(m) + 1*(m==0)
    if n == 0:
        Znmx1 = np.zeros(np.shape(rho))
        Znmy1=Znmx1
        bnm = np.zeros((1,3))
    else:
        narr =  np.array([(n-1),(n-1),(n-2)*(n>=2)])
        marr = np.array([m-1,m+1,m])
        Nn = Nnm(n,m)*np.ones([1,3])
        Nd = Nnm(narr,marr)
        #print(narr,marr)
        bnm = Nn/Nd
        #print(bnm)
        nZarr = narr[:2]
        mZarr = np.array([a*np.abs(m-1),a*np.abs(m+1)])
        Znmx1 = n*(bnm[:,0]*Znm(rho,theta,nZarr[0],mZarr[0]) + a*np.sign(m+1)*bnm[:,1]*Znm(rho,theta,nZarr[1],mZarr[1]))
        Znmy1 = n*(-a*np.sign(m-1)*bnm[:,0]*Znm(rho,theta,nZarr[0],-mZarr[0]) + bnm[:,1]*Znm(rho,theta,nZarr[1],-mZarr[1]))
    return  Znmx1, Znmy1, bnm[:,2]

def dZ_IC(rho,theta,m):
    # Return initial condition for recursively findin drivatives of Zernike:
    n=m
    if n >= 0:
        dxZn = Nnm(n,n)*n*rho**(n-1)*Theta_nm(theta,n-1)*(-n==1)
        dyZn = -Nnm(n,n)*n*rho**(n-1)*Theta_nm(theta,-n+1)*(n==1)
    else:
        n=np.abs(n)
        dxZn = Nnm(n,n)*n*rho**(n-1)*Theta_nm(theta,-n+1)*(n==1)
        dyZn = Nnm(n,n)*n*rho**(n-1)*Theta_nm(theta,n-1)*(-n==1)
        #print(dxZn,dyZn)
    return dxZn, dyZn

def Zderivs(x,y,n,m):
    rho, phi = cart2pol(x,y)
    # Acquire initial condition, check for value of m and acquire the first state to initialise iteration
    dZxIC, dZyIC = dZ_IC(rho,phi,m) 
    # Implement recursive process for evaulation:
    nstep = int((n-abs(m))/2)
    n_recurse = np.arange(abs(m),n+1,2)
    dZnmdx = dZxIC
    dZnmdy = dZyIC
    #print(n_recurse)
    for iN in n_recurse:
        #print(iN)
        idZnmdx,idZnmdy, bnm2 = Zderivs_Znm(rho,phi,iN,m)
        dZnmdx = bnm2*dZnmdx
        dZnmdy = bnm2*dZnmdy
        dZnmdx += idZnmdx
        dZnmdy += idZnmdy 
    return dZnmdx,dZnmdy

def Hnm_construct(x,y,j):
    # Construct a  2k x  j matrix of zernike derivatives in x and y:
    Hnm = np.zeros([2*len(x),j])
    # Generat all of n,m indices up order j:
    n,m = Zernike_nm_indices(j)
    # Looping through all indices to generate matrix of derivative:
    for ij in range(j):
        iZdx, iZdy = Zderivs(x,y,n[ij],m[ij])
        Hnm[::2,ij] = iZdx
        Hnm[1::2,ij]= iZdy
    return Hnm

def Zernike_solve(S,Hnm):
    # Pseudo-inverse Hnm matrix:
    Hinv = np.linalg.pinv(Hnm)
    Bcoeff = np.matmul(Hinv,S)
    return Bcoeff

def compute_Zernike_from_hsg(hsgradients,i,R0=None):
    ''' Compute Zernike coefficients from HS gradients, 
    assuming that centre of the pupil is at center of gradient map
    Input:
        hsgradient: HS_Gradients instance
        R0: Radius of region to be normalised for Zernike coeff 
        i: maximum order of Zernike polynomial coeff to reconstruct
    Output
        b coefficient
    '''
    grads = hsgradients.gradients[:,:2]
    cents = hsgradients.gradients[:,2:]
    
    if R0 is None:
        Rpupil = np.min([abs(cents[:,0].min()),cents[:,0].max(),abs(cents[:,1].min()),cents[:,1].max()])
    else:
        Rpupil = R0
    cents = cents/Rpupil
    grads = grads*Rpupil
    # Construct slope vector:
    S = np.empty([2*len(cents[:,0])])
    S[::2] = grads[:,0]
    S[1::2] = grads[:,1]
    # Construct H matrix
    Hmat = Hnm_construct(cents[:,0],cents[:,1],i)
    # Solve for Zernike coefficients:
    bcoeff = Zernike_solve(S,Hmat)
    return bcoeff

def construct_slope_vector(hsgradients, R0=None):
    grads = hsgradients.gradients[:,:2].copy()
    cents = hsgradients.gradients[:,2:].copy()
    
    if R0 is None:
        Rpupil = np.min([abs(cents[:,0].min()),cents[:,0].max(),abs(cents[:,1].min()),cents[:,1].max()])
    else:
        Rpupil = R0
    cents = cents/Rpupil
    grads = grads*Rpupil
    # Construct slope vector:
    S = np.empty([2*len(cents[:,0])])
    S[::2] = grads[:,0]
    S[1::2] = grads[:,1]
    return S, cents

def bar_plot_zernike(bnm, i=None, logplot = False, title = 'Zernike mode decomposition'):
    ''' Plotting bar graph from zernike coefficient:'''
    if i is None:
        iplot = 14
    else:
        if i>len(bnm):
            raise Exception('number of orders to plot must be less than number of coefficients defined')
        else:
            iplot = i
    n,m = Zernike_nm_indices(iplot)
    n = n[:,0]
    xticklabel = []
    for iN,iM in zip(n,m):
        xticklabel.append(str(int(iN))+','+str(int(iM)))
    bcoeffnm = 1e9*bnm[:iplot]
    
    if logplot is True:
        bp_max10 = np.max(np.sign(bcoeffnm)*np.ceil(np.log10(abs(bcoeffnm))))
        bp_min10 = np.min(np.sign(bcoeffnm)*np.ceil(np.log10(abs(bcoeffnm))))
        # generate interval
        iNval = []
        for iNeg in np.arange(bp_min10,0):
            iNval.append(np.linspace(-10**abs(iNeg),-2*10**(abs(iNeg)-1),9))
        iNval = np.ravel(np.asarray(iNval))
        zeroNval = np.linspace(-1,1,3)
        iPval = []
        for iPos in np.arange(0,bp_max10):
            iPval.append(np.linspace(2*10**(abs(iPos)),10**(abs(iPos)+1),9))
        iPval = np.ravel(np.asarray(iPval))
        yticks = np.hstack([iNval,zeroNval,iPval])
        
        fig=plt.figure(figsize=(10,6))
        ax = fig.add_subplot(111)
        ax.bar(np.arange(len(bcoeffnm))+1,bcoeffnm, width=0.5)
        ax.set_yscale('symlog')
        ax.set_ylabel('Zernike coefficient amplitude (nm)')
        xticks = np.arange(1,iplot+1)
        plt.yticks(yticks)
        plt.xticks(xticks, xticklabel)
        plt.xlabel('Zernike mode (n,m)')
        ax.grid(True,which='minor',linestyle='-')
        ax.grid(True,which='major',linestyle='--')
        plt.title(title)
        plt.show()
    else:
        fig=plt.figure(figsize=(10,6))
        ax = fig.add_subplot(111)
        ax.bar(np.arange(len(bcoeffnm))+1, bcoeffnm, width =.5)
        xticks = np.arange(1,iplot+1)
        plt.xticks(xticks, xticklabel)
        plt.xlabel('Zernike mode (n,m)')
        ax.set_ylabel('Zernike coefficient amplitude (nm)')
        ax.grid(True,which='minor',linestyle='-')
        ax.grid(True,which='major',linestyle='--')
        plt.title(title)
        plt.show()  
        
def construct_spherical_gradient(R,cents):
    S = 2/R
    gradx = (S)*cents[:,0]
    grady = (S)*cents[:,1]
    gradients = np.vstack([gradx,grady]).transpose()
    return gradients

def construct_residual_gradient(hsg,R):
    cents = hsg.gradients[:,2:]
    grads = hsg.gradients[:,:2]
    S_grads = construct_spherical_gradient(R,cents)
    grad_res = grads- S_grads
    hsc_ref = HS_Centroids()
    hsc_res = HS_Centroids()
    hsc_ref.centroids = hsg.gradients[:,2:]/(hsg.pixel_size*hsg.magnification)+hsg.origin
    mod_factor = hsg.pixel_size /(hsg.lever_arm * hsg.magnification)
    displacement = grad_res/mod_factor
    hsc_res.centroids=hsc_ref.centroids+displacement

    hsg_res = HS_Gradients(hsc_ref,hsc_res)
    hsg_res.pixel_size = hsg.pixel_size
    hsg_res.magnification = hsg.magnification
    hsg_res.lever_arm = hsg.lever_arm
    hsg_res.construct_gradients()
    return hsg_res

def construct_wf(r,phi,bcoeff):
    i = len(bcoeff)
    n,m = Zernike_nm_indices(i)
    WFrms = np.zeros(np.shape(r)) 
    for ij in range(i):
        WFrms+= bcoeff[ij]*Znm(r,phi,n[ij],m[ij])
    return WFrms


def compute_rms_error_from_zernike(bcoeff):
    WF_rms = sqrt((bcoeff**2).sum())
    return WF_rms

def compute_rms_error_from_hsg(hsg,R,i):
    '''Compute wavefront rms error (flat pupil) using gradient data 
    and compute deviation from best fit'''
    # First compute residual gradient:
    hsg_res = construct_residual_gradient(hsg,R)
    # Solve for Zernike coefficients from hsg_res:
    bcoeff_res = compute_Zernike_from_hsg(hsg_res,i)
    # Compute wf rms error from zernike coefficient:
    wf_rms = compute_rms_error_from_hsg(bcoeff)
    return wf_rms

def compute_Gauss_rms_error_from_hsg(hsg,R,i,w0, R0 = None):
    ''' Compute Gaussian weighted rms error from using gradient data
    ande rms error from best fit:
    Input:
        hsg: HS_gradient instance containing gradient with center ofe deformation assume to be at origin (0,0)
        R:  radius of curvature of the best fit spherical wavefront
        w0: beam radius of target Gaussian beam for weighting
        i: Number of order to be used for wavefront reconstruction,
        R0: radius of pupil to be normalised
    Output: 
        WFrms_error_Gauss: Gaussian weighted wavefront rms error
    '''
    grads = hsg.gradients[:,:2].copy()
    cents = hsg.gradients[:,2:].copy()
    if R0 is None:
        Rpupil = np.min([abs(cents[:,0].min()),cents[:,0].max(),abs(cents[:,1].min()),cents[:,1].max()])
    else:
        Rpupil = R0
    # First compute residual gradient :
    hsg_res = construct_residual_gradient(hsg,R)
    # Solve for Zernike coefficients from hsg_res:
    bcoeff_res = compute_Zernike_from_hsg(hsg_res,i,Rpupil)
    # Reconstruct wavefront from bcoeff_res:
    nsample = 500
    dr = 1/nsample
    dphi = 2*np.pi/nsample
    r = np.arange(0,1,dr)
    phi = np.arange(0,2*np.pi,dphi)
    R, PHI = np.meshgrid(r,phi)
    WF_res =  construct_wf(R,PHI,bcoeff_res)
    # Compute gaussian weighted residual wavefront rms error:
    w0_norm = w0/Rpupil
    Gauss = sqrt(2/(np.pi*w0_norm**2))*exp(-(R**2)/(w0_norm**2))
    WFrms_remnsqred_Gauss_int = ((WF_res**2)*R*dr*dphi*Gauss**2).sum()
    Norm_factor = (Gauss**2*R*dr*dphi).sum()
    WFrms_error_Gauss = sqrt(WFrms_remnsqred_Gauss_int/np.pi)/Norm_factor
    return  WFrms_error_Gauss
