from numpy import *
import ctypes
import time
import os

class HS_Zyla:
    """A class to operate an ANDOR Zyla SCMOS camera.

    This class uses ``ctypes`` Python module to load ANDOR SDK3 library
    files (in c++) and gain access to camera control functions and various
    camera parameters.

    Prior to using this class, the camera must be connected and any
    required drivers must be installed. A user should then ensure that the
    camera is successfully detected and operable using ANDOR utilities. See
    ANDOR SDK# manual for more details.

    Once the camera is ready for use, this class requires two .so files
    that come with the SDK: libatcore and libatutility. Please see the
    descriptions of the class :class:`HS_AndorSDK3 <HWS.HS_Zyla.HS_AndorSDK3>`
    for more details.

    This class is written so that a user can write scripts to control the
    camera as well as access the camera parameters without having to worry
    about setting c/c++ datatypes for the variables.

    **Instantiation**

    Instantiation process consists of two steps: 

    (1) setting up feature types and errors, and 

    (2) providing access to ANDOR SDK3 and establishing a camera connection.

    "Feature" is a term used in ANDOR SDK3 that includes the variables that
    represent the camera parameters as well as some camera operation
    commands (called "command features" in SDK). Each parameter has also
    its data type, and the method of accessing those parameters vary
    according to their datatypes.

    Although ANDOR SDK3 has different access methods for each datatype, by
    setting up a dictionary array of feature names as keys and feature data
    types as values, instance methods this class is written so that a user
    can retrieve the parameter values (and change them when applicable)
    without having to specify the data type. For more details, see the
    descriptions of :func:`~HS_Zyla.get` and :func:`~HS_Zyla.set`.

    **Class Variables**

    - ``sdk3``: the entry point to accessing the methods in the SDK3 core and
      utility libraries.

      :Type: an instance of :class:`HS_AndorSDK3 <HWS.HS_Zyla.HS_AndorSDK3>`
      :Updated by: :func:`~HS_Zyla.setup_sdk`

    - ``feature_types``: a *dict* of the camera features and their data types.

      :Type: *dict*
      :Updated by: :func:`~HS_Zyla.set_types`

    - ``feature_errors``:

    
    **Camera Parameters**


    **Camera Functions**

    """

    sdk3 = None
    feature_types = {}
    feature_errors = {}
    master_folder = '/home/ozgrav/camera_folder/'

    def __init__(self, device_no=0, camera_folder=None, check_status=True):
        # camera_folder is not being used at the moment, as a user can
        # directly acquire an image (whether a single frame or an average
        # of multiple frames) without saving to a disk.
        #
        # I may later implement automatic camera folder creation etc., if
        # found necessary.
        self.sdk3 = None
        self.handle = None
        self.camera_folder = None
        self.frame_folder = None
        self.frame_rate = None
        self.bad_pixels_data = None
        self.resolution = (2048,2048)
        # the duration for AT_WaitBuffer to wait for a frame (in ms)
        self.wait_duration = 10

        if bool(HS_Zyla.feature_types) is False:
            self.set_types()

        if bool(HS_Zyla.feature_errors) is False:
            self.set_errors()

        # self.setup_sdk()
        self.connect()
        self.set_camera_folder()
        self.set("ExposureTime", 0.013)
        self.set("CycleMode", "Continuous")

    def take_and_average_frames(self, no_of_frames=1):
        isb = self.get("ImageSizeBytes")
        stride = self.get("AOIStride")
        pixel_encoding = self.get("PixelEncoding")
        pixel_no = self.resolution[0]*self.resolution[1]
        
        raw_buffer = ascontiguousarray(empty(isb, dtype=uint8))
        frame_arr = ascontiguousarray(empty(pixel_no, dtype=uint16))
        raw_ptr = ctypes.c_void_p()
        buffer_size = ctypes.c_longlong()

        accframe_1D = zeros(self.resolution[0]*self.resolution[1])

        self.run_command("AcquisitionStart")
        time.sleep(0.1)

        for i in range(no_of_frames):
            self.try_function(self.sdk3.core.AT_QueueBuffer,
                              self.handle,
                              ctypes.c_void_p(raw_buffer.ctypes.data),
                              ctypes.c_int(raw_buffer.size))

            self.try_function(self.sdk3.core.AT_WaitBuffer,
                              self.handle,
                              ctypes.byref(raw_ptr),
                              ctypes.byref(buffer_size),
                              self.wait_duration)

            self.try_function(self.sdk3.utility.AT_ConvertBuffer,
                              raw_ptr,
                              ctypes.c_void_p(frame_arr.ctypes.data),
                              ctypes.c_long(self.resolution[0]),
                              ctypes.c_long(self.resolution[1]),
                              ctypes.c_long(stride),
                              ctypes.c_wchar_p(pixel_encoding),
                              ctypes.c_wchar_p("Mono16"))

            accframe_1D = accframe_1D + frame_arr.copy()

            if ((i+1) % 100) == 0:
                print i+1

        accframe_2D = accframe_1D.reshape(self.resolution[1],
                                          self.resolution[0]).astype(float)

        self.try_function(self.sdk3.core.AT_Flush, self.handle)
        self.run_command("AcquisitionStop")
        return accframe_2D/no_of_frames


    def set_camera_folder(self):
        csn = self.get("SerialNumber")
        mpath = os.path.normpath(HS_Zyla.master_folder)
        # cf = HS_Zyla.master_folder + csn
        cf = os.path.join(os.sep, mpath, csn)

        if os.path.exists(mpath):
            if not os.path.exists(cf):
                print('The camera folder ' + csn + ' does not exist. ' + \
                      'Creating...')
                os.makedirs(cf)
                print('Created the camera folder: ' + cf + '/')
        else:
            print('The master folder' + mpath + \
                  ' does not exist. ' + 'Creating...')
            os.makedirs(mpath)
            print('Created the master folder: ' + mpath)
            os.makedirs(cf)
            print('Created the camera folder: ' + cf)

        self.camera_folder = cf
        
    def generate_file_names(self, folder, fprefix, no_of_frames, ext='npy'):
        file_names = []
        zero_pad = max(4, len(str(no_of_frames)))
        frame_folder = os.path.normpath(folder)

        if '.' in ext:
            fext = ext.replace('.', '')
        else:
            fext = ext

        for ii in range(no_of_frames):
            idx = ii + 1
            fn = fprefix + str(idx).zfill(zero_pad) + '.' + fext
            fpfn = os.path.join(os.sep, frame_folder, fn)
            file_names.append(fpfn)

        return file_names
    
    def take_and_save_frames(self, no_of_frames=1, folder=None, fprefix='frame_'):
        if folder is None:
            # folder = self.camera_folder + 'frames/'
            folder = os.path.join(os.sep, self.camera_folder, 'frames')
        self.frame_folder = folder

        if not os.path.exists(folder):
            print('The folder named ' + folder + ' does not exist. ' + \
                  'Creating...')
            os.makedirs(folder)
            print('Created the folder: ' + folder)

        fnames = self.generate_file_names(folder, fprefix, no_of_frames)
        isb = self.get("ImageSizeBytes")
        stride = self.get("AOIStride")
        pixel_encoding = self.get("PixelEncoding")
        pixel_no = self.resolution[0]*self.resolution[1]
        
        raw_buffer = ascontiguousarray(empty(isb, dtype=uint8))
        frame_arr = ascontiguousarray(empty(pixel_no, dtype=uint16))
        raw_ptr = ctypes.c_void_p()
        buffer_size = ctypes.c_longlong()

        accframe_1D = zeros(self.resolution[0]*self.resolution[1])

        self.run_command("AcquisitionStart")
        time.sleep(0.1)

        for i in range(no_of_frames):
            self.try_function(self.sdk3.core.AT_QueueBuffer,
                              self.handle,
                              ctypes.c_void_p(raw_buffer.ctypes.data),
                              ctypes.c_int(raw_buffer.size))

            self.try_function(self.sdk3.core.AT_WaitBuffer,
                              self.handle,
                              ctypes.byref(raw_ptr),
                              ctypes.byref(buffer_size),
                              self.wait_duration)

            self.try_function(self.sdk3.utility.AT_ConvertBuffer,
                              raw_ptr,
                              ctypes.c_void_p(frame_arr.ctypes.data),
                              ctypes.c_long(self.resolution[0]),
                              ctypes.c_long(self.resolution[1]),
                              ctypes.c_long(stride),
                              ctypes.c_wchar_p(pixel_encoding),
                              ctypes.c_wchar_p("Mono16"))

            accframe_1D = accframe_1D + frame_arr.copy()

            frame_2D = frame_arr.reshape(self.resolution[1],
                                         self.resolution[0])

            save(fnames[i], frame_2D)
            
            if ((i+1) % 100) == 0:
                print i+1

        accframe_2D = accframe_1D.reshape(self.resolution[1],
                                          self.resolution[0]).astype(float)

        self.try_function(self.sdk3.core.AT_Flush, self.handle)
        self.run_command("AcquisitionStop")
        return accframe_2D/no_of_frames

    def try_feature(self, function, *args):
        oc = function(*args)

        # if bool(args):
        #     print function.__name__, args[1].value
        # else:
        #     print function.__name__

        if oc == 0:
            return
        else:
            self.raise_error(oc)

    def try_function(self, function, *args):
        oc = function(*args)

        if oc == 0:
            return
        else:
            self.raise_error(oc)

    def raise_error(self, oc):
        text = HS_Zyla.feature_errors[str(oc)]['text']
        err_name = HS_Zyla.feature_errors[str(oc)]['name']

        raise Exception (err_name + ": " + text)
        
    def setup_sdk(self):
        if HS_Zyla.sdk3 is None:
            HS_Zyla.sdk3 = HS_AndorSDK3()
            # print type(HS_Zyla.sdk3.core)
            # print type(HS_Zyla.sdk3.utility)

        sdk3 = HS_Zyla.sdk3
        self.try_function(sdk3.core.AT_InitialiseLibrary)
        self.try_function(sdk3.utility.AT_InitialiseUtilityLibrary)

        self.sdk3 = HS_Zyla.sdk3

    def clear_sdk(self):
        if self.sdk3 is None:
            pass
        else:
            self.try_function(self.sdk3.core.AT_FinaliseLibrary)
            self.try_function(self.sdk3.utility.AT_FinaliseUtilityLibrary)
            self.sdk3 = None

    def connect(self):
        self.setup_sdk()
        
        if self.handle is None:
            self.handle = ctypes.c_void_p()
            self.try_function(self.sdk3.core.AT_Open,
                              0,
                              ctypes.byref(self.handle))
        else:
            self.check_connection()

    def disconnect(self):
        self.try_function(self.sdk3.core.AT_Flush,
                          self.handle)
        self.try_function(self.sdk3.core.AT_Close,
                          self.handle)
        self.clear_sdk()
        self.handle = None

    def check_connection(self):
        pass
    
    def set(self, name, value):
        ftype = HS_Zyla.feature_types[name]

        if ftype == "integer":
            self.set_integer_feature(name, value)
        elif ftype == "float":
            self.set_float_feature(name, value)
        elif ftype == "enumerated":
            self.set_enumerated_feature(name, value)
        elif ftype == "boolean":
            self.set_boolean_feature(name, value)
        elif ftype == "string":
            self.set_string_feature(name, value)
        elif ftype == "command":
            self.run_command(name)

    def set_integer_feature(self, name, value):
        self.try_feature(self.sdk3.core.AT_SetInt,
                         self.handle,
                         ctypes.c_wchar_p(name),
                         ctypes.c_longlong(value))

    
    def set_float_feature(self, name, value):
        self.try_feature(self.sdk3.core.AT_SetFloat,
                         self.handle,
                         ctypes.c_wchar_p(name),
                         ctypes.c_double(value))

    
    def set_enumerated_feature(self, name, value):
        # To avoid the segmentation fault that can occur when value is not
        # string, I shall force it to be string for now. May need better
        # way to handle the situation.
        value = str(value)
        self.try_feature(self.sdk3.core.AT_SetEnumeratedString,
                         self.handle,
                         ctypes.c_wchar_p(name),
                         ctypes.c_wchar_p(value))

    
    def set_boolean_feature(self, name, value):
        self.try_feature(self.sdk3.core.AT_SetBool,
                         self.handle,
                         ctypes.c_wchar_p(name),
                         ctypes.c_bool(value))

    def set_string_feature(self, name, value):
        self.try_feature(self.sdk3.core_AT_SetString,
                         self.handle,
                         ctypes.c_wchar_p(name),
                         self.c_wchar_p(value))
    
    def get(self, name):
        ftype = HS_Zyla.feature_types[name]

        if ftype == "integer":
            return self.get_integer_feature(name)
        elif ftype == "float":
            return self.get_float_feature(name)
        elif ftype == "enumerated":
            return self.get_enumerated_feature(name)
        elif ftype == "boolean":
            return self.get_boolean_feature(name)
        elif ftype == "string":
            return self.get_string_feature(name)
        elif ftype == "command":
            self.run_command(name)

    def get_integer_feature(self, name):
        v = ctypes.c_longlong()

        self.try_feature(self.sdk3.core.AT_GetInt,
                         self.handle,
                         ctypes.c_wchar_p(name),
                         ctypes.byref(v))
        return v.value

    def get_float_feature(self, name):
        v = ctypes.c_double()

        self.try_feature(self.sdk3.core.AT_GetFloat,
                         self.handle,
                         ctypes.c_wchar_p(name),
                         ctypes.byref(v))
        return v.value

    def get_enumerated_feature_wk(self, name):
        # WK: Don't understand why this does not work...
        
        ei = ctypes.c_longlong()

        self.try_feature(self.sdk3.core.AT_GetEnumIndex,
                         self.handle,
                         ctypes.c_wchar_p(name),
                         ctypes.byref(ei))

        # print ei.value

        # WK: this somehow does not appear to matter
        #
        # ec = ctypes.c_longlong()

        # self.try_feature(self.sdk3.core.AT_GetEnumCount,
        #                  self.handle,
        #                  ctypes.c_wchar_p(name),
        #                  ctypes.byref(ec))

        # print ec.value

        v = ctypes.c_wchar_p(" "*256)
        
        self.try_feature(self.sdk3.core.AT_GetEnumStringByIndex,
                         self.handle,
                         ctypes.c_wchar_p(name),
                         ei,
                         ctypes.byref(v),
                         ctypes.c_int(256))

        return v.value

    def get_enumerated_feature(self, name):
        ei = ctypes.c_longlong()

        self.try_feature(self.sdk3.core.AT_GetEnumIndex,
                         self.handle,
                         ctypes.c_wchar_p(name),
                         ctypes.byref(ei))

        char_size = 256

        v = ctypes.c_wchar_p(' '*char_size)
        
        self.try_feature(self.sdk3.core.AT_GetEnumStringByIndex,
                         self.handle,
                         ctypes.c_wchar_p(name),
                         ctypes.c_longlong(ei.value),
                         v,
                         ctypes.c_int(char_size))

        return v.value
                         

    def get_string_feature(self, name):
        s = ctypes.c_int()

        self.try_feature(self.sdk3.core.AT_GetStringMaxLength,
                         self.handle,
                         ctypes.c_wchar_p(name),
                         ctypes.byref(s))

        v = ctypes.c_wchar_p(' '*s.value)

        self.try_feature(self.sdk3.core.AT_GetString,
                         self.handle,
                         ctypes.c_wchar_p(name),
                         v,
                         s)

        return v.value

    def get_boolean_feature(self, name):
        v = ctypes.c_bool()

        self.try_feature(self.sdk3.core.AT_GetBool,
                         self.handle,
                         ctypes.c_wchar_p(name),
                         ctypes.byref(v))
                         
        return v.value
    

    def run_command(self, name):
        self.try_feature(self.sdk3.core.AT_Command,
                         self.handle,
                         ctypes.c_wchar_p(name))


    # def take_frames(self, folder=None, no_of_frames=1):
    #     pass

    # def take_single_frame(self):
    #     pass


    def set_types(self):

        integer_features = \
            [ "AccumulateCount",
              "AOIHBin",
              "AOIHeight",
              "AOILeft",
              "AOIStride",
              "AOITop"
              "AOIVBin",
              "AOIWidth",
              "Baseline",
              "BufferOverflowEvent",
              "DeviceVideoIndex",
              "EventsMissedEvent",
              "ExposedPixelHeight",
              "ExposureEndEvent",
              "ExposureStartEvent",
              "FrameCount",
              "ImageSizeBytes",
              "LUTValue",
              "MultitrackCount",
              "MultitrackEnd",
              "MultitrackSelector",
              "MultitrackStart",
              "RowNExposureEndEvent",
              "RowNExposureStartEvent",
              "SensorHeight",
              "SensorWidth",
              "TimestampClock",
              "TimestampClockFrequency" ]

        float_features = \
            [ "BytesPerPixel",
              "ExposureTime",
              "ExternalTriggerDelay",
              "FrameRate",
              "LineScanSpeed",
              "LUTIndex",
              "MaxInterfaceTransferRate",
              "PixelHeight",
              "PixelWidth",
              "ReadoutTime",
              "RowReadTime",
              "SensorTemperature",
              "ShutterTransferTime" ]

        enumerated_features = \
            [ "AOIBinning",
              "AOILayout",
              "AuxiliaryOutSource",
              "AuxOutSourceTwo",
              "Bitdepth",
              "CycleMode",
              "ElectronicShutteringMode",
              "EventSelector",
              "FanSpeed",
              "IOSelector",
              "PixelEncoding",
              "PixelReadoutRate",
              "SensorReadoutMode",
              "ShutterOutputMode",
              "SimplePreAmpGainControl",
              "TemperatureStatus",
              "TriggerMode" ]

        string_features = \
            [ "CameraModel",
              "CameraName",
              "ControllerID",
              "FirmwareVersion",
              "InterfaceType",
              "SerialNumber",
              "SoftwareVersion" ]

        boolean_features = \
            [ "AlternatingReadoutDirectionr",
              "CameraAcquiring",
              "CameraPresent",
              "EventEnable",
              "FastAOIFrameRateEnable",
              "FullAOIControl",
              "IOInvert",
              "MetadatEnable",
              "MetadataFrame",
              "MetadataTimestamp",
              "MultitrackBinned",
              "Overlap",
              "RollingShutterGlobalClear",
              "ScanSpeedControlEnable",
              "SensorCooling",
              "SpuriousNoiseFilter",
              "StaticBlemishCorrection",
              "VerticallyCentreAOI" ]

        command_features = \
            [ "AcquisitionStart",
              "AcquisitionStop",
              "CameraDump",
              "SoftwareTrigger",
              "TimestampClockReset" ]

        ft = HS_Zyla.feature_types
        
        for f in integer_features:
            ft[f] = "integer"

        for f in float_features:
            ft[f] = "float"

        for f in enumerated_features:
            ft[f] = "enumerated"    

        for f in string_features:
            ft[f] = "string"

        for f in boolean_features:
            ft[f] = "boolean"

        for f in command_features:
            ft[f] = "command"

    def set_errors(self):
        fe = {}

        fe['0'] = { "name": "AT_SUCCESS",
                    "text": "Function call has been successful." }

        fe['2'] = { "name": "AT_ERR_NOTIMPLEMENTED",
                    "text": "Feature has not been implemented " + \
                            "for the chosen camera." }

        fe['3'] = { "name": "AT_ERR_READONLY",
                    "text": "Feature is read only." }

        fe['4'] = { "name": "AT_ERR_NOTREADABLE",
                    "text": "Feature is currently not readable." }

        fe['5'] = { "name": "AT_ERR_NOTWRITABLE",
                    "text": "Feature is currently not writable." }
        
        fe['6'] = { "name": "AT_ERR_OUTOFRANGE",
                    "text": "Value is outside the maximum " + \
                            "and minimum limits." }

        fe['7'] = { "name": "AT_ERR_INDEXNOTAVAILABLE",
                    "text": "Index is currently not available." }

        fe['8'] = { "name": "AT_ERR_INDEXNOTIMPLEMENTED",
                    "text": "Index is not implemented for the " + \
                            "chosen camera." }

        fe["9"] = { "name": "AT_ERR_EXCEEDEDMAXSTRINGLENGTH",
                    "text": "String Value provided exceeds the maximum " + \
                            "allowed length." }

        fe['10'] = { "name": "AT_ERR_CONNECTION",
                     "text": "Error connecting to or disconnecting " + \
                             "from hardware." }
        
        fe['11'] = { "name": "AT_ERR_NO_DATA",
                     "text": "No Internal Event or Internal Error." }
        
        fe['12'] = { "name": "AT_ERR_INVALIDHANDLE",
                     "text": "Invalid device handle passed to function." }

        fe['13'] = { "name": "AT_ERR_TIMEOUT",
                     "text": "The AT_WaitBuffer function timed out " + \
                             "while waiting for data to arrive in " + \
                             "output queue."}

        fe['14'] = { "name": "AT_ERR_BUFFERFULL",
                     "text": "The input queue has reached its capacity." }

        fe['15'] = { "name": "AT_ERR_INVALIDSIZE",
                     "text": "The size of a queued buffer did not " + \
                             "match the frame size." }

        fe['16'] = { "name": "AT_ERR_INVALIDALIGNMENT",
                     "text": "A queued buffer was not aligned on an " + \
                             "8-byte boundary." }

        fe['17'] = { "name": "AT_ERR_COMM",
                     "text": "An error has occurred while communicating " + \
                             "with hardware." }

        fe['18'] = { "name": "AT_ERR_STRINGNOTAVAILABLE",
                     "text": "Index / String is not available." }

        fe['19'] = { "name": "AT_ERR_STRINGNOTIMPLEMENTED",
                     "test": "Index / String is not implemented for " + \
                             "the chosen camera." }

        fe['20'] = { "name": "AT_ERR_NULL_FEATURE",
                     "text": "NULL feature name passed to function."}

        fe['22'] = { "name": "AT_ERR_NULL_IMPLEMENTED_VAR",
                     "text": "Feature not implemented." }

        fe['23'] = { "name": "AT_ERR_NULL_READABLE_VAR",
                     "text": "Readable not set." }

        fe['25'] = { "name": "AT_ERR_NULL_WRITABLE_VAR",
                     "text": "Writable not set." }

        fe['26'] = { "name": "AT_ERR_NULL_MINVALUE",
                     "text": "NULL min value." }

        fe['27'] = { "name": "AT_ERR_NULL_MAXVALUE",
                     "text": "NULL max value." }

        fe['28'] = { "name": "AT_ERR_NULL_VALUE",
                     "text": "NULL value returned from function." }

        fe['30'] = { "name": "AT_ERR_NULL_COUNT_VAR",
                     "text": "NULL feature count." }

        fe['31'] = { "name": "AT_ERR_NULL_ISAVAILABLE_VAR",
                     "text": "Available not set." }

        fe['33'] = { "name": "AT_ERR_NULL_EVCALLBACK",
                     "text": "EvCallBack parameter is NULL." }

        fe['34'] = { "name": "AT_ERR_NULL_QUEUE_PTR",
                     "text": "Pointer to queue is NULL." }

        fe['35'] = { "name": "AT_ERR_NULL_WAIT_PTR",
                     "text": "Wait pointer is NULL." }

        fe['36'] = { "name": "AT_ERR_NULL_PTRSIZE",
                     "text": "Pointer size is NULL." }

        fe['37'] = { "name": "AT_ERR_NOMEMORY",
                     "text": "No memory has been allocated for " + \
                             "the current action." }

        fe['38'] = { "name": "AT_ERR_DEVICEINUSE",
                     "text": "Function failed to connect to a device " + \
                             "because it is already being used." }

        fe['100'] = { "name": "AT_ERR_HARDWARE_OVERFLOW",
                      "text": "The software was not able to retrieve " + \
                              "data from the card or camera fast " + \
                              "enough to avoid the internal hardware " + \
                              "buffer busting." }

        HS_Zyla.feature_errors = fe

class HS_AndorSDK3:

    def __init__(self, library_folder="/usr/local/lib"):
        self.libver = "3.13.30001.0"
        self.core = ctypes.cdll.LoadLibrary(library_folder + os.sep + \
                                            "libatcore.so." + \
                                            self.libver)
        self.utility = ctypes.cdll.LoadLibrary(library_folder + os.sep + \
                                               "libatutility.so." + \
                                               self.libver)

if (__name__ == "__main__"):
    # Some test code. Can be changed to suit one's needs.
    cam = HS_Zyla()
    frame_s1 = cam.take_and_average_frames(1000)
    frame_s2 = cam.take_and_average_frames(1000)
    print frame_s1.max(), frame_s2.max()

# Won Kim 29/05/2018 2:48:27 PM
