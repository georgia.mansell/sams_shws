from numpy import *
from HS_Centroids import *
from HS_WFP import *

class HS_WFP_Zonal(HS_WFP):

    def __init__(self, hsg=None, order=6):
        HS_WFP.__init__(self,hsg,order)
        centres = None
        origins = None
        widths = None
        
    def calculate_coeffs_single_zone(self,centre,width,origin, magnification):
        rcents = self.hsgradients.initial_centroids.centroids
        separr = abs(rcents - centre)
        ci = where(logical_and((separr[:,0] < width/2),(separr[:,1] < width/2)))[0]
        nci = size(ci)

        if size(ci) < 1:
            print 'There are no centroids within a specified region,'
        else:
            cents_i = rcents[ci,:]
            cents_f = self.hsgradients.final_centroids.centroids[ci,:]

            hsc_i = HS_Centroids()
            hsc_i.no_of_centroids = nci
            hsc_i.centroids = cents_i

            hsc_f = HS_Centroids()
            hsc_f.no_of_centroids = nci
            hsc_f.centroids = cents_f

            hsg = HS_Gradients(hsc_i,hsc_f)
            hsg.origin = origin
            hsg.magnification = magnification
            hsg.construct_gradients()

            wfp = HS_WFP(hsg,self.order)
            wfp.to_unitize = self.to_unitize
            wfp.unitization_length = self.unitization_length
            wfp.compute_poly_coeffs()
            wfp.compute_seidel_coeffs()

        return wfp
    
    def calculate_coeffs(self):
        centres = self.centres
        widths = self.widths
        origins = self.origins
        nc = shape(centres)[0]
        wfps = []
        for k in arange(nc):
            wfps.append(self.calculate_coeffs_single_zone(centres[k,:],
                                                          widths[k],
                                                          origins[k]))

        return wfps
        
