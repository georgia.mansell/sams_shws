import os
import subprocess as sp
from socket import gethostname
from numpy import *
from datetime import datetime, timedelta
import pickle as pickle
from time import strftime, gmtime

from HS_CamConfig import *
from HS_Image import *

class HS_Export:
    #A class to export data to file.

    def __init__(self, IFO='H1', optic='ETMY_HWS', frameDir='/home/controls/temp'):
        self.avGradients = None
        self.avGpstime = 0
        self.avImage = None
        self.numberOfAvg = 0
        self.lastSaveTime = datetime.now()
        self.averageDuration = 20
        self.IFO = IFO
        self.optic = optic
        self.frameDir = frameDir
        self.timeDir = None
        self.filename = None
        self.hostname = gethostname()
        

    def update_averages(self, gradients, image, gpstime):
        # update the averages to be saved
        if self.numberOfAvg == 0:
            self.avGradients = gradients
            self.avGpstime = gpstime
            self.avImage = image
            
            self.numberOfAvg = 1
        else:
            self.avGradients = self.avGradients + gradients
            self.avGpstime = self.avGpstime + gpstime
            self.avImage = self.avImage + image
            
            self.numberOfAvg =  self.numberOfAvg + 1       
            
    def export_if_necessary(self):
        # export the data to file if enough time has elapsed
        tdelta = datetime.now() - self.lastSaveTime
        
        if self.numberOfAvg > 0 and (tdelta.total_seconds() > self.averageDuration):
            # get the averages
            self.avGradients = self.avGradients/self.numberOfAvg
            self.avGpstime = self.avGpstime/self.numberOfAvg
            self.avImage = self.avImage/self.numberOfAvg
            self.avImage = self.avImage.astype('uint16')
            
            # udpate the save time so it is stored in the file
            self.lastSaveTime = datetime.now()
            
            # create the time directory string and check it exists
            self.timeDir = str(int(floor(self.avGpstime/1E5)*1E5)) + '/'
            dirSub = self.frameDir + '/' + self.IFO + '/' + \
                     self.optic + '/' + self.timeDir
            
            if os.path.exists(dirSub) == False:
                # create the sub directory
                os.makedirs(dirSub)
            
            # create the filename and export the data
            self.filename = dirSub + \
                            str(int(round(self.avGpstime))) + '_' + self.IFO + \
                            '_' + self.optic + '_HS_Export.p'
            pickle.dump(self,open(self.filename,"wb"),2)
            print(self.filename)
            print('Saved file - ' + strftime("%a, %d %b %Y %H:%M:%S +0000", gmtime()) + ' UTC')
                
            # reset the averages
            self.numberOfAvg = 0
                
                
                
                
                
