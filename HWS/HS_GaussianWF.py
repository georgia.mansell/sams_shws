from numpy import *

class HS_GaussianWF:
    """A simple calss to generate a Gaussian WF and the corresponding gradients.

    This class is used mainly in simulations of detecting anomalies on a
    wavefront aberration map, where an anomaly is modeled as a small width
    2D Gaussian.

    """
    def __init__(self):
        self.amplitude = None
        self.width = None
        self.gradients = None
        self.wf = None
        self.wf_full = None
        self.centroids = None
        self.centre = array([511.5, 511.5])*12e-6

    def construct_gradients(self):
        ctr = zeros(shape(self.centroids))
        ctr[:,0] = self.centroids[:,0] - self.centre[0]
        ctr[:,1] = self.centroids[:,1] - self.centre[1]

        self.construct_wf()

        self.gradients = zeros(shape(ctr))
        self.gradients[:,0] = self.wf * (2/self.width**2) * ctr[:,0]
        self.gradients[:,1] = self.wf * (2/self.width**2) * ctr[:,1]

    def construct_wf(self):
        ctr = zeros(shape(self.centroids))
        ctr[:,0] = self.centroids[:,0] - self.centre[0]
        ctr[:,1] = self.centroids[:,1] - self.centre[1]

        self.wf = self.amplitude * \
                  exp(-(ctr[:,0]**2 + ctr[:,1]**2)/self.width**2)

# Won Kim 7/11/2018 4:37:52 PM
