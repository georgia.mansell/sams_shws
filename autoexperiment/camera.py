from pyueye import ueye
import time
import ctypes
import numpy as np

class Camera:
    def __init__(self):
        #Initialize c variables
        self.hcam = ueye.HIDS(0)
        self.pccmem = ueye.c_mem_p()
        self.memID = ueye.c_int()
        self.bits = ueye.INT(8)
        sensorinfo = ueye.SENSORINFO()
        rectAOI = ueye.IS_RECT()

        #Establish connection to the camera
        nRet = ueye.is_InitCamera(self.hcam, None)
        if nRet != ueye.IS_SUCCESS:
            print("is_InitCamera ERROR. Cannot establish connection")

        ueye.is_SetDisplayMode(self.hcam, ueye.IS_SET_DM_DIB)
        ueye.is_GetSensorInfo(self.hcam, sensorinfo)

        nRet = ueye.is_AOI(self.hcam, ueye.IS_AOI_IMAGE_GET_AOI, rectAOI, ueye.sizeof(rectAOI))
        if nRet != ueye.IS_SUCCESS:
            print("is_AOI ERROR")

        self.width = rectAOI.s32Width
        self.height = rectAOI.s32Height

        #Allocate image memory
        nRet = ueye.is_AllocImageMem(self.hcam, self.width, self.height ,self.bits, self.pccmem, self.memID)
        if nRet != ueye.IS_SUCCESS:
            print("is_AllocImageMem ERROR")
        ueye.is_SetImageMem(self.hcam, self.pccmem, self.memID)
        ueye.is_SetColorMode(self.hcam, ueye.IS_CM_MONO8)

    def start_camera(self):
        self.pitch = ueye.INT()
        # Activates the camera's live video mode (free run mode)
        nRet = ueye.is_CaptureVideo(self.hcam, ueye.IS_DONT_WAIT)
        if nRet != ueye.IS_SUCCESS:
            print("is_CaptureVideo ERROR")

        # Enables the queue mode for existing image memory sequences
        nRet = ueye.is_InquireImageMem(self.hcam, self.pccmem, self.memID, self.width, self.height, self.bits, self.pitch)
        if nRet != ueye.IS_SUCCESS:
            print("is_InquireImageMem ERROR")

    def set_expo(self,exposure):
        expo = ctypes.c_double(exposure)
        nRet = ueye.is_Exposure(self.hcam, ueye.EXPOSURE_CMD.IS_EXPOSURE_CMD_SET_EXPOSURE, expo, 8)
        if nRet != ueye.IS_SUCCESS:
            print("is_InquireImageMem ERROR")
        else:
            print("Exposure set to: ",expo.value)

    def avg_frames(self,nFrames):
        sum = np.zeros((self.height.value,self.width.value))
        for i in range(nFrames):
            # Get frame from camera
            array = ueye.get_data(self.pccmem, self.width, self.height, self.bits, self.pitch, copy=False)
            # Add it to sum
            sum += np.reshape(array,(self.height.value, self.width.value))
        # Calculate average
        sum = sum/nFrames
        return sum
    
    def save_img(self,filename):
        nret = ueye.is_FreezeVideo(self.hcam, ueye.IS_WAIT)
        print(nret)
        FileParams = ueye.IMAGE_FILE_PARAMS()
        FileParams.pwchFileName = filename + ".png"
        FileParams.nFileType = ueye.IS_IMG_PNG
        FileParams.ppcImageMem = None
        FileParams.pnImageID = None
        nret = ueye.is_ImageFile(self.hcam, ueye.IS_IMAGE_FILE_CMD_SAVE, FileParams, ueye.sizeof(FileParams))
        print(nret)

    def cleanup(self):
        ueye.is_FreeImageMem(self.hcam, self.pccmem, self.memID)
        ueye.is_ExitCamera(self.hcam)
