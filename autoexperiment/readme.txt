This program automates gathering samples from the SHWS. It controls the voltage across the piezo element as well as optionally exposing a reference mirror in between samples.

The program needs the following python libraries:
pyueye: communication with WFS camera.
    pip3 install pyueye

pyvisa: communication with signal generator.
    pip3 install pyvisa-py

pyserial: communication with arduino board that controls servos.
    pip3 install pyserial

imageio: save acquired images.
    pip3 install imageio

Also, the IDS imaging suite must be installed for pyueye to work properly.
https://en.ids-imaging.com/download-ueye-win64.html

The line:
ser = Servos('/dev/ttyUSB0')
in main.py should match the serial port the Arduino is connected to.

Note: this program was only tested in Linux as of 2019-12-09.

