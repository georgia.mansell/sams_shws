print('Automated WFS measurement program')
print('For help with setup read readme.txt')

import numpy as np
import imageio
import matplotlib.pyplot as plt

import time
import os
from datetime import datetime

from camera import Camera
from servos import Servos
from funcgen import Funcgen

print('Connecting to peripherals...')
cam = Camera()
ser = Servos('/dev/ttyUSB0')
fun = Funcgen()

expose_ref = False
if input('Expose flat mirror? (y/n)')=='y':
    expose_ref = True
v_updown = False
if input('Voltage up and down? (y/n)')=='y':
    v_updown = True

print('\nPlease enter parameters')

voltage_min = float(input('MIN VOLTAGE: '))
voltage_max = float(input('MAX VOLTAGE: '))
voltage_step = float(input('STEP VOLTAGE: '))
print('\n')

volts = np.linspace(voltage_min,voltage_max,(voltage_max-voltage_min)//voltage_step+1)
print(str(volts))

cam.set_expo(1.2)
cam.start_camera()

ts = str(datetime.now().strftime('%Y-%m-%d %H:%M:%S')) + 'u'
print('Creating folder: ' + ts)
os.mkdir(ts)
ser.move('sam')
time.sleep(3)
print('#### STARTING EXPERIMENT ####')
print('----UP----')

for v in volts:
    v_str = str(v)+'V'
    print(v_str)

    #Set target voltage, take gain and offset into account
    v_gain = 29.4
    v_offset = 0.36
    fun.voltage((v-v_offset)/v_gain)
    time.sleep(1)
    
    if expose_ref:
        #Expose reference mirror, block sample
        ser.move('ref')
        time.sleep(3)

        t = time.time()
        #Get image of reference mirror, average
        image = cam.avg_frames(100)
        print("    Ref took ",time.time()-t," seconds")
        #plt.imshow(image)
        #plt.show()
        imageio.imwrite('./'+ts+'/'+v_str+'_ref.png', image.astype(np.uint8))

        #Expose sample mirror, block reference
        ser.move('sam')
        time.sleep(3)

    t = time.time()
    #Get image of sample mirror, average
    image = cam.avg_frames(100)
    print("    Sample took ",time.time()-t," seconds")
    imageio.imwrite('./'+ts+'/'+v_str+'_sam.png', image.astype(np.uint8))

if v_updown:
    volts = np.flip(volts)
    ts = str(datetime.now().strftime('%Y-%m-%d %H:%M:%S')) + 'd'
    print('Creating folder: ' + ts)
    os.mkdir(ts)

    print('---DOWN---')
    for v in volts:
        v_str = str(v)+'V'
        print(v_str)

        #Set target voltage, take gain and offset into account
        v_gain = 29.4
        v_offset = 0.36
        fun.voltage((v-v_offset)/v_gain)

        if expose_ref:
            #Expose reference mirror, block sample
            ser.move('ref')
            time.sleep(3)

            t = time.time()
            #Get image of reference mirror, average
            image = cam.avg_frames(100)
            print("    Ref took ",time.time()-t," seconds")
            #plt.imshow(image)
            #plt.show()
            imageio.imwrite('./'+ts+'/'+v_str+'_ref.png', image.astype(np.uint8))

            #Expose sample mirror, block reference
            ser.move('sam')
            time.sleep(3)

        t = time.time()
        #Get image of sample mirror, average
        image = cam.avg_frames(100)
        print("    Sample took ",time.time()-t," seconds")
        imageio.imwrite('./'+ts+'/'+v_str+'_sam.png', image.astype(np.uint8))

print('#### END ####')
cam.cleanup()