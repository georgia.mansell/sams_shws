import serial
import time

class Servos:
    def __init__(self,port):
        self.ard = serial.Serial(port,115200)  # open serial port
        print(self.ard.name)         # check which port was really used
        time.sleep(2)
        self.ard.write(b'123')

    def move(self,pos):
        if pos == 'ref':
            self.ard.write(b'b')
        elif pos == 'sam':
            self.ard.write(b'a')

