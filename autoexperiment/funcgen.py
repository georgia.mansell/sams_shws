import pyvisa
import time

class Funcgen:
    def __init__(self):
        rm = pyvisa.ResourceManager()
        print(rm.list_resources())
        self.inst = rm.open_resource('USB0::2391::1031::MY44036143::0::INSTR')

    def voltage(self,v):
        self.inst.write('APPL:DC 1 HZ, 3.0 VPP, '+str(v)+' V')
